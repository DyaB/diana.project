package curs.week11.syncronize;

public class SyncMultiMethods {
    static Counter sharedCounter = new Counter();
    static Double lock1 = new Double(1);
    static Double lock2 = new Double(2);

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new IncreaseRunnable());
        Thread t2 = new Thread(new IncreaseRunnable());
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        System.out.println("counter : " + sharedCounter);
    }
}

class IncreaseRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            SyncMultiMethods.sharedCounter.plus(1);
            SyncMultiMethods.sharedCounter.minus(1);
        }
        System.out.println("Runnable finished, counter value is: " + SyncMultiMethods.sharedCounter);
    }

}

class Counter {
    private int count = 0;

    synchronized void plus(int x) {
        count += x;
    }

    void minus(int x) {
        synchronized (SyncMultiMethods.lock1) {
            count -= x;
        }
    }

    @Override
    public String toString() {
        return "Counter{" +
                "count=" + count +
                '}';
    }
}

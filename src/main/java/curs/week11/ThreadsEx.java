package curs.week11;

public class ThreadsEx {
    public static void main(String[] args) {
        Runnable r1 = new CustomRunnable("Runnable-1");
        Runnable r2 = new CustomRunnable("Runnable-2");
        Runnable r3 = new CustomRunnable("Runnable-3");
        Runnable r4 = new CustomRunnable("Runnable-4");
        Runnable r5 = new CustomRunnable("Runnable-5");

        Thread t1 = new Thread(r1,"thread-for-1");
        Thread t2 = new Thread(r2,"thread-for-2");
        Thread t3 = new Thread(r3,"thread-for-3");
        Thread t4 = new Thread(r4,"thread-for-4");
        Thread t5 = new Thread(r5,"thread-for-5");

        //thread separate
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}

class CustomRunnable implements Runnable{
    private String name;

    public CustomRunnable(String name) {
        this.name = name;
        System.out.println(name + ": Created");
    }

    @Override
    public void run() {
        System.out.println(name + ": Start running...");
        try{
            for(int i=1; i<=3 ; i++){
                System.out.println(name + ": " + "-thread running ");
                Thread.sleep(5000);
            }
        } catch (InterruptedException e) {
            System.out.println("interrupted");
        }
    }
}
package curs.week11;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

public class StreamSets {
    public static void main(String[] args) {
        //ce au in comun, ce au diferit, impreuna
        Set<String> set1 = new HashSet<>(Arrays.asList("aa", "bb", "cc"));
        Set<String> set2 = new HashSet<>(Arrays.asList("bb", "cc", "dd"));
        Set<String> resultIntersection = set1.stream().filter(s -> set2.contains(s)).collect(toSet());
        System.out.println("Intersection:" + resultIntersection);
        Set<String> resultDifference = set2.stream().filter(s -> !set1.contains(s)).collect(toSet());
        System.out.println("Difference: " +  resultDifference);
     //  Stream<Stream<String>> ss = Stream.of(set1.stream(), set2.stream());
        System.out.println(Stream.concat(set1.stream(),set2.stream()).collect(toList()));
    }
}

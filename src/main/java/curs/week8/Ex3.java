package curs.week8;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.IntStream;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.util.stream.Collectors.*;

public class Ex3 {
    public static void main(String[] args) {

        //String path = "./doc/s4_week8_tema1_files.txt";
        String path = "./doc/books/java_books_recommendations.txt";
        File file = new File(path);

        //fileScn - to read from file
        Scanner fileScn = null;
        try {
            fileScn = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.err.println("No file found");
        }

        displayLineNumberAndContent(file, fileScn);
        displayLongestLine(file);
        totalNumberOfLines(file);
        numberOfWords(file);
        System.out.println(wordsAndOccurrence(file));
        mostUsedNrWords(file, 8);
        System.out.println("Longest word in " + file.getName() + " is: " + longestWord(file));
    }

    //read nrLines lines, display line number and its content
    static void displayLineNumberAndContent(File file, Scanner fileScn) {
        System.out.println("Give number of lines to read: ");
        //keyScn - to read from keyboard
        Scanner keyScn = new Scanner(System.in);
        int nrLines = keyScn.nextInt();
        int lineCount = 0;
        assert fileScn != null;
        while (lineCount < nrLines && fileScn.hasNextLine()) {
            String line = fileScn.nextLine();
            lineCount++;
            System.out.println(lineCount + ": " + line);
        }
    }


    // find&display longest line & its line number & length
    //for the entire file
    static void displayLongestLine(File file) {
        Map<Integer, String> mapLines = mapLineNrAndContentValue(file);
        if (mapLines.entrySet().stream().findFirst().isPresent()) {
            Map.Entry<Integer, String> max = mapLines.entrySet().stream().findFirst().get();
            int lineNr = 1;
            for (Map.Entry m : mapLines.entrySet()) {
                if (max.getValue().length() < m.getValue().toString().length()) {
                    max.setValue(m.getValue().toString());
                    lineNr = (Integer) m.getKey();
                }
            }
            System.out.println("Longest line is nr: " + lineNr + " of length: " + max.getValue().length() + " and content: \"" + max.getValue() + "\"");
        }
    }

    //create a map that holds as key the line number and its content as value, for all the lines
    static Map<Integer, String> mapLineNrAndContentValue(File file) {
        Scanner fileScn2 = null;
        try {
            fileScn2 = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.err.println("No file found");
        }
        int allcount = 0;
        Map<Integer, String> mapLines = new HashMap<>();
        assert fileScn2 != null;
        while (fileScn2.hasNextLine()) {
            String line = fileScn2.nextLine();
            allcount++;
            mapLines.put(allcount, line);
        }
        return mapLines;
    }

    //number of lines
    static void totalNumberOfLines(File file) {
        Map<Integer, String> mapLines = mapLineNrAndContentValue(file);
        System.out.println("There are " + mapLines.size() + " lines in file " + file.getName());
    }

    //list of all the words from the file indicated
    static List<String> listOfWords(File file) {
        Map<Integer, String> mapLines = mapLineNrAndContentValue(file);
        ArrayList<String> allWords = new ArrayList<>();
        for (Map.Entry map : mapLines.entrySet()) {
            String currentLine = map.getValue().toString();
            if (currentLine.length() > 0) {
                //p{Punct} instead of !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~  \s for whitespace and + for matching one or more of the previous
                String[] words = currentLine.split("[\\p{Punct}\\s]+");
                for (String word : words) {
                    if (word.length() > 0)
                        allWords.add(word);
                }
            }
        }
        return allWords;
    }

    //number of words
    static void numberOfWords(File file) {
        System.out.println("Number of words: " + listOfWords(file).size());
    }

    //map with (distinct)words as keys and their occurrence as value
    static Map<String, Integer> wordsAndOccurrence(File file) {
        List<String> listOfWords = listOfWords(file);
        Map<String, Integer> mapOfWords = new HashMap<>();
        for (String s : listOfWords) {
            int i = 1;
            if (mapOfWords.containsKey(s)) {
                i = mapOfWords.get(s);
                i++;
                mapOfWords.put(s, i);
            }
            mapOfWords.putIfAbsent(s, i);
        }
        return mapOfWords;
    }

    //top nr most used words
    static void mostUsedNrWords(File file, int nr) {

        ValueComparator valueComparator = new ValueComparator(wordsAndOccurrence(file));
        Map<String, Integer> sortedMap = new TreeMap<>(valueComparator);
        sortedMap.putAll(wordsAndOccurrence(file));
        List<String> words = sortedMap.keySet().stream().limit(nr).collect(toList());
        System.out.println("Most used " + nr + " words in file " + file.getName() + " are: " + words);
    }

    //longest word
    static String longestWord(File file){
        return listOfWords(file).stream().max(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if(o1.length()>o2.length()) return 1;
                if(o1.length()<o2.length()) return -1;
                return 0;
            }
        }).get();
    }
}

//custom comparator for sorting after values in map
class ValueComparator implements Comparator<String> {

    Map<String, Integer> base;

    public ValueComparator(Map<String, Integer> base) {
        this.base = base;
    }

    public int compare(String a, String b) {
        if (base.get(a) >= base.get(b)) return -1;
        else return 1;
    }
}

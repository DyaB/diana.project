package curs.week8.week8.prezentare.files;

import java.io.*;

/**
 * Example of working with byte/character IO streams
 */
public class FileStreamsExample {

    public static void main(String[] args) throws IOException {

        String baseDir = "."; //use a relative path to get the base folder of this project
        //we could also have used an absolute path, like: String baseDir = "c:\\wantsome";

        //this (path with \\) works on Windows, but NOT Linux/MacOS !
        //String fileToCopy = baseDir + "\\src\\main\\java\\curs\\week8\\prezentare\\files\\FileStreamsExample.java";

        //this (path with /) should work on both Windows/Linux! (if not, try the more officially right way of using File.separator instead of '\\' or '/')
        String sourceFile = baseDir + "/src/main/java/curs/week8/prezentare/files/FileStreamsExample.java";
        System.out.println("Trying to copy file: " + new File(sourceFile).getAbsolutePath());

        String destinationFile = sourceFile + "_COPIED_with_char_buffer.txt";

        //Test the copy methods:
        copyUsingCharStreamsWithBuffer(sourceFile, destinationFile);

        //copyUsingByteStreamsOneByteAtATime(...)
        //copyUsingByteStreamsWithBuffer(...);

        System.out.println("Done, check that file was copied (" + destinationFile + ")");
    }

    /**
     * Example of copy file using a byte stream and reading/writing one byte at a time.
     * The code is simpler, but is less efficient/slower! (compared to using a buffer)
     */
    private static void copyUsingByteStreamsOneByteAtATime(String inputFilePath, String outputFilePath) throws IOException {

        //using regular try-finally (needs cleanup in finally block, and to declare the variables outside the try)
        FileInputStream in = null;
        FileOutputStream out = null;
        try {
            in = new FileInputStream(inputFilePath);
            out = new FileOutputStream(outputFilePath);

            int c;
            while ((c = in.read()) != -1) { //read one byte at a time, until finished
                out.write(c); //and write it to out stream
            }

        } finally {
            if (in != null) in.close();
            if (out != null) out.close();
        }
    }

    /**
     * Copy a file using a byte stream and a buffer
     */
    private static void copyUsingByteStreamsWithBuffer(String inputFilePath, String outputFilePath) throws IOException {

        //using try-with-resources:
        try (FileInputStream in = new FileInputStream(inputFilePath);
             FileOutputStream out = new FileOutputStream(outputFilePath)) {

            byte[] buffer = new byte[1024]; //1kb buffer
            int bytesRead = -1;

            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
        } //at end of try the resources (in,out) will be auto closed
    }

    /**
     * Copy a file using a character stream and a buffer
     */
    private static void copyUsingCharStreamsWithBuffer(String inputFilePath, String outputFilePath) throws IOException {

        //using try-with-resources:
        try (FileReader in = new FileReader(inputFilePath);
             FileWriter out = new FileWriter(outputFilePath)) {

            char[] buffer = new char[1024];
            int charsRead = -1;

            while ((charsRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, charsRead);
            }
        } //at end of try the resources (in,out) will be auto closed
    }
}

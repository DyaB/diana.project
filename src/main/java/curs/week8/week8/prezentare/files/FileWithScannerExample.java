package curs.week8.week8.prezentare.files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Example of using java.util.Scanner class to read from console and/or from a file.
 */
public class FileWithScannerExample {
    public static void main(String[] args) throws FileNotFoundException {

        String path = "src/main/java/curs/week8/prezentare/files/FileWithScannerExample.java"; //current file!

        File file = new File(path);
        if (!file.exists()) {
            System.err.println("Error: file not found: " + file.getAbsolutePath());
            return;
        }
        System.out.println("Input file exists: " + file.getAbsolutePath());

        System.out.print("Give max number of lines to show: ");
        Scanner keyScanner = new Scanner(System.in);
        int maxLines = keyScanner.nextInt();

        Scanner fileScanner = new Scanner(file);

        System.out.println("Showing first " + maxLines + " lines of the file " + file.getAbsolutePath() + ":");
        int lineCount = 0;
        while (lineCount < maxLines && fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            lineCount++;
            System.out.println("[" + lineCount + "] " + line);
        }
    }
}

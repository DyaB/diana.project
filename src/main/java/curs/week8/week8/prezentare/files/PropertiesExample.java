package curs.week8.week8.prezentare.files;

import java.io.*;
import java.util.Properties;

public class PropertiesExample {

    public static void main(String[] args) throws IOException {
        writeProperties("config.properties");
        readProperties("config.properties");
    }

    static void writeProperties(String fileName) throws IOException {
        Properties prop = new Properties();
        prop.setProperty("key1", "value1");
        prop.setProperty("key2", "value2");

        System.out.println("Writing properties to: " + fileName);
        try (OutputStream out = new FileOutputStream(fileName)) {
            prop.store(out, "some comments here");
        }
    }

    static void readProperties(String fileName) throws IOException {
        System.out.println("Reading properties from: " + fileName);
        Properties prop = new Properties();
        try (InputStream in = new FileInputStream(fileName)) {
            prop.load(in);
        }
        System.out.println("Loaded properties: " + prop);
    }
}

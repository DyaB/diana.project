package curs.week8.week8.prezentare.files;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class StandardStreamsExample {

    public static void main(String[] args) throws IOException {
        //withRegularTryFinally();
        withTryWithResources();
    }

    private static void withRegularTryFinally() throws IOException {
        InputStreamReader in = null;
        OutputStreamWriter out = null;
        try {
            in = new InputStreamReader(System.in);
            out = new OutputStreamWriter(System.out);

            System.out.println("Enter characters (+Enter), 'E' to exit");

            char c;
            do {
                c = (char) in.read();
                out.write(c); //somehow equivalent to: System.out.print(c);
                out.flush();  //needed to flush/show data after each Enter
            } while (c != 'E');

        } finally { //always need to cleanup (close the streams) at the end!
            if (in != null) in.close();
            if (out != null) out.close();
        }
    }

    private static void withTryWithResources() throws IOException {

        //try-with-resources:
        try (InputStreamReader in = new InputStreamReader(System.in);
             OutputStreamWriter out = new OutputStreamWriter(System.out)) {

            System.out.println("Enter characters (+Enter), 'E' to exit");

            char c;
            do {
                c = (char) in.read();
                out.write(c); //equivalent to: System.out.print(c);
                out.flush();  //needed to flush/show data after each Enter
            } while (c != 'E');
        } //at the end of try the resources (in,out) will be auto closed!
    }
}

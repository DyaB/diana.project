package curs.week8.week8.prezentare.files;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Example of working with zip archived files
 */
public class ZipFileExample {

    public static void main(String[] args) throws IOException {
        writeZipFileContentsToFile("F:\\java_books.zip", "F:\\java_books_list.txt");
    }

    /**
     * Gets the names of the files from a zip archive,
     * and writes this list to a specified text file.
     */
    static void writeZipFileContentsToFile(String zipFileName, String outputFileName) throws IOException {

        Path outputFilePath = Paths.get(outputFileName);

        //open zip file and create output file with try-with-resources statement
        try (ZipFile zf = new ZipFile(zipFileName);
             BufferedWriter writer = Files.newBufferedWriter(outputFilePath, StandardCharsets.UTF_8)) { //use utility class Files to easy create writer

            //enumerate each entry in zip file
            for (Enumeration entries = zf.entries(); entries.hasMoreElements(); ) {
                //get the entry name and write it to the output file
                String newLine = System.getProperty("line.separator");
                String zipEntryName = ((ZipEntry) entries.nextElement()).getName() + newLine;
                writer.write(zipEntryName, 0, zipEntryName.length());
            }
        }
    }
}

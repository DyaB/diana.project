package curs.week8.week8.prezentare.files;

import java.io.File;
import java.util.Arrays;
import java.util.Date;

/**
 * Example of listing contents/scanning all subfolders and files of a given base folder
 */
public class ListDirectoryTreeExample {

    public static void main(String[] args) {
        String basePath = ".";

        String allFilesInfo = fileAndChildrenInfo(basePath);

        System.out.println("All files under '" + basePath + "' :");
        System.out.println(allFilesInfo);
    }

    public static String fileAndChildrenInfo(String path) {
        File file = new File(path);

        //build line for file
        String fileInfo = file.getAbsolutePath() + "  -> ";
        fileInfo += file.isDirectory() ? "[DIR]" : "[file]";
        fileInfo += file.isHidden() ? " (hidden!)" : "";
        fileInfo += ", " + file.length() + " bytes, last update: " + new Date(file.lastModified());
        fileInfo += "\n";

        //add all lines for each children
        if (file.isDirectory()) {
            String[] childrenNames = file.list(); //get the list of names of direct children (files or dirs)
            Arrays.sort(childrenNames); //sort child names alphabetically
            for (String childName : childrenNames) {
                String childFullPath = file.getAbsolutePath() + File.separator + childName;
                fileInfo += fileAndChildrenInfo(childFullPath);
            }
        }
        return fileInfo;
    }
}

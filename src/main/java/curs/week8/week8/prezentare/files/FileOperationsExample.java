package curs.week8.week8.prezentare.files;

import java.io.File;
import java.util.Date;

public class FileOperationsExample {

    public static void main(String[] args) {

        showFileInfo("./src/main/java/curs/week8/prezentare/files/FileOperationsExample.java");

        //String path = "C:\\dir1\\dir2"; //a windows style path
        //String path = "~/dir1/dir2"; //a linux style path
        String path = "dir1" + File.separator + "dir2"; //relative, portable

        createDirs(path);
        showFileInfo(path);

        listDirContents(path + "/..");

        //cleanup - delete the new dir (only dir2 now)
        new File(path).delete();
        new File("dir1").delete(); //delete also its parent (created also by us)
    }

    private static void showFileInfo(String path) {
        File file = new File(path);
        System.out.println("\nDisplaying info about file '" + path + "':");
        System.out.println(" - absolute path: " + file.getAbsolutePath()); //absolute path (even if file doesn't exist)
        System.out.println(" - file exists:   " + file.exists()); //check if file exists (is accessible)
        System.out.println(" - is directory:  " + file.isDirectory()); //check if is dir or regular
        System.out.println(" - size (bytes):  " + file.length()); //file size (in bytes)
        System.out.println(" - last modified (ms): " + file.lastModified()); //date of last update (type 'long' !)
        System.out.println(" - last modified (as date): " + new Date(file.lastModified()).toString()); //date in readable format

        //other methods:
        //canExecute(), canRead(), canWrite() - operations allowed for file
        //list(), listFiles() - list containing files (if this file is a dir)
        //renameTo(String) - rename this file
        //delete() - deletes this file
    }

    private static boolean checkFileExists(String filePath) {
        File file = new File(filePath);
        System.out.println(" - file '" + filePath + "' (absolute path: '" + file.getAbsolutePath() + "') exists?: " + file.exists());
        return file.exists();
    }

    private static void createDirs(String filePath) {
        System.out.println("\nCreating dirs for: '" + filePath + "':");
        if (!checkFileExists(filePath)) {
            File file = new File(filePath);
            System.out.println("   - creating folder(s): " + file.getAbsolutePath());
            file.mkdirs(); //creates the directory (including missing parents)
        } else {
            System.out.println("   - skipped creating folder, as it already exists: " + filePath);
        }
    }

    private static void listDirContents(String filePath) {
        File file = new File(filePath); // create new file object

        String[] paths = file.list(); // get child files/folders

        System.out.println("\nContents of folder '" + file.getAbsolutePath() + "':");
        for (String path : paths) { // for each name in the path array
            System.out.println("  " + path); // prints file path
        }
    }
}

package curs.week8.week8.prezentare.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Example of doing a simple HTTP GET request only using Java standard classes.
 * <p>
 * For more details see: https://www.baeldung.com/java-http-request
 */
public class UrlExample {

    public static void main(String[] args) throws IOException {
        performGetRequest();
    }

    private static void performGetRequest() throws IOException {
        URL url = new URL("https://www.wikipedia.org/");

        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        int responseCode = con.getResponseCode(); //read the response code
        System.out.println("Response Code : " + responseCode);

        //also read the response body
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine + "\n");
            }
            System.out.println(response.toString());
        }
    }
}

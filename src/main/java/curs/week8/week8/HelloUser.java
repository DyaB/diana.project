package curs.week8.week8;

import java.io.*;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

/**
 * Solution for exercise 1 from assignment for week8, done in class
 */
public class HelloUser {

    //define some constants
    private final static String CONFIG_FILENAME = "config.properties";
    private final static String CONFIG_KEY_USERNAME = "username";
    private final static String CONFIG_KEY_LAST_VISIT_TIME = "last_visit_time";


    public static void main(String[] args) {

        //define first a Properties instance (we'll use it both for read and write the config)
        Properties config = new Properties();

        //try to load data from properties file in our Properties object (if it fails, Properties remains empty)
        tryLoadFromFile(config);

        //declare a variable for user name (needed in message) and try to set it's value somehow
        String userName;

        //if data was loaded from config file and we found our expected key for username
        if (config.containsKey(CONFIG_KEY_USERNAME)) {
            userName = config.getProperty(CONFIG_KEY_USERNAME); //then get the value for that key from the config, and use it as username
        } else { //data not loaded or user key is missing
            //ask the user for its name instead
            System.out.print("Please enter your name: ");
            Scanner scn = new Scanner(System.in);
            userName = scn.next();
            //need to also save this new name to our Properties object (to be saved later to file)
            config.setProperty(CONFIG_KEY_USERNAME, userName);
        }

        //try to also get the time of last visit from Properties object (loaded from file if present there)
        String lastVisitTimeAsString = config.getProperty(CONFIG_KEY_LAST_VISIT_TIME);
        if (lastVisitTimeAsString != null) { //means we have a String value loaded from config file for this key
            long lastVisitTimeAsLong = Long.valueOf(lastVisitTimeAsString); //transform the String value to a number of type long (should be possible)
            Date lastVisitTimeAsDate = new Date(lastVisitTimeAsLong); //then convert the long value to a Date value, for nicer display
            System.out.println("Hello " + userName + ", nice to see you again! (last visit time: " + lastVisitTimeAsDate + ")");
        } else { //null value, meaning file or key was missing, so we show a simplified message without it
            System.out.println("Hello " + userName + ", nice to meet you!");
        }

        //also update the last visit time to the current time (in same format, as a long number of milliseconds since 1970)
        long currentTime = new Date().getTime(); //get now in millis
        config.setProperty(CONFIG_KEY_LAST_VISIT_TIME, String.valueOf(currentTime)); //put value under the time key in our Properties instance

        //save the updated Properties instance (with updated values for time and possible username keys) to the file, to be used next time
        trySaveToFile(config);
    }

    /**
     * Loads configuration from the config file from disk
     *
     * @param config a Properties object into which to add the data loaded from file
     */
    private static void tryLoadFromFile(Properties config) {
        try (InputStream in = new FileInputStream(CONFIG_FILENAME)) {
            config.load(in);
            System.out.println("Success loading data from config file: " + config);
        } catch (IOException io) {
            System.err.println("Config file missing.");
        }
    }

    /**
     * Saves the given configuration (as a Properties instance) to the configuration file on disk
     */
    private static void trySaveToFile(Properties config) {
        try (OutputStream out = new FileOutputStream(CONFIG_FILENAME)) {
            config.store(out, "Holds config for HelloUser.java");
        } catch (IOException e) {
            System.err.println("Error saving properties to file '" + CONFIG_FILENAME + "', cause: " + e);
        }
    }
}

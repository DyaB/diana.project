package curs.week8.week8;

import java.io.File;
import java.util.Date;
import java.util.Scanner;

public class Exe2 {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        System.out.println("File name: ");
        String path = scn.next();

        File file = new File(path);
        System.out.println(" Absolute path for file is: " +
                file.getAbsolutePath());
        System.out.println(" File is on the disc: " + file.exists());
        System.out.println(" File is a dir: " + file.isDirectory());
        System.out.println(" File size: " + file.length());
        System.out.println(" Last update time: " + file.lastModified());
        System.out.println(" File is writable: " + file.canWrite());


        long lastTime = file.lastModified();
        Date date = new Date(lastTime);
        System.out.println(date);
    }


}

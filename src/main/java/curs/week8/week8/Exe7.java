package curs.week8.week8;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Exe7 {

    public static void main(String[] args) {
        String path = "./doc";
        String pattern = "WEek4";

        File file = new File(path);

//        File foundFile = findFirst(file, pattern, "");
//        if (foundFile == null) {
//            System.out.println("File not found");
//        } else {
//            System.out.println("File found: " + foundFile.getAbsolutePath());
//        }

//        System.out.println("Files count: " + countFiles(file));
//        System.out.println("Files & Directories count: " + countStuff(file));
        List<File> foundFiles = findAll(new File("."), "WEEK7", "");
        for (File f : foundFiles) {
            System.out.println(f.getAbsolutePath());
        }
    }

    private static File findFirst(File parent, String pattern, String prefix) {

        System.out.println(prefix + "Checking parent folder: " + parent.getAbsolutePath());
        File[] files = parent.listFiles();
        for (File child : files) {
            System.out.println(prefix + "Checking childName : " + child.getName());
            if (child.getName().toLowerCase().contains(pattern.toLowerCase())) {
                System.out.println(prefix + "Found good name :" + child.getName());
                return child;
            }
            if (child.isDirectory()) {
                System.out.println(prefix + "Child is directory, checking is children " + child.getName());

                File foundChild = findFirst(child, pattern, prefix + "  ");

                if (foundChild != null) {
                    System.out.println(prefix + "Foud good nephew : " + foundChild.getName());
                    return foundChild;
                }
            }
        }
        System.out.println(prefix + "Found no matching children for :" + parent.getName());
        return null;
    }

    private static List<File> findAll(File parent, String pattern, String prefix) {
        List<File> result = new ArrayList<>();

        if (parent.getName().toLowerCase().contains(pattern.toLowerCase())) {
            System.out.println(prefix + "Found good name :" + parent.getName());
            result.add(parent);
        }

        if (parent.isDirectory()) {
            System.out.println(prefix + "Checking parent folder: " + parent.getAbsolutePath());

            File[] files = parent.listFiles();
            for (File child : files) {
                System.out.println(prefix + "Checking childName : " + child.getName());

                List<File> foundNephews = findAll(child, pattern, prefix + "  ");

                result.addAll(foundNephews);
            }
        }
        return result;
    }

    private static int countFiles(File parent) {

        System.out.println(parent.getAbsolutePath() + " start counting files...");
        if (parent.isFile()) {
            System.out.println(parent.getAbsolutePath() + " is a file");
            return 1;
        }

        int count = 1;
        for (File child : parent.listFiles()) {
            count += countFiles(child);
        }
        System.out.println(parent.getAbsolutePath() + " has " + count + "files");
        return count;
    }

    private static Counts countStuff(File parent) {

        System.out.println(parent.getAbsolutePath() + " start counting files...");

        if (parent.isFile()) {
            System.out.println(parent.getAbsolutePath() + " is a file");
            return new Counts(1, 0);
        }

        Counts parentCounts = new Counts(0, 1);
        for (File child : parent.listFiles()) {
            Counts childCounts = countStuff(child);
            parentCounts.dirCount += childCounts.dirCount;
            parentCounts.fileCount += childCounts.fileCount;
        }
        System.out.println(parent.getAbsolutePath() + " has " + parentCounts);
        return parentCounts;
    }
}

class Counts {
    int fileCount;
    int dirCount;

    public Counts(int fileCount,
                  int dirCount) {
        this.fileCount = fileCount;
        this.dirCount = dirCount;
    }

    @Override
    public String toString() {
        return "Counts{" +
                "fileCount=" + fileCount +
                ", dirCount=" + dirCount +
                '}';
    }
}


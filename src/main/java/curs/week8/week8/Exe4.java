package curs.week8.week8;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

public class Exe4 {
    public static void main(String[] args) {
        String path = "./doc";
        File file = new File(path);

        File[] files = file.listFiles();
        Arrays.sort(files, new FileComparator());

        long totalSize = 0;
        for (File f : files) {
            System.out.println(f.length() + "," + f.getName());
            totalSize += f.length();
        }
        System.out.println(" Total size is: " + totalSize);
    }

}

class FileComparator implements Comparator<File> {

    @Override
    public int compare(File f1, File f2) {
        //compare two directory by name;
        if (f1.isDirectory() && f2.isDirectory()) {
            return f1.getName().compareTo(f2.getName());
        }
        //compare two files by size( descending);
        if (f1.isFile() && f2.isFile()) {
            if (f1.length() > f2.length()) {
                return -1;
            } else if (f1.length() < f2.length()) {
                return 1;
            } else {
                return 0;
            }
        }
        //when comparing a file with a dir--> dir small;

        if (f1.isFile() && f2.isDirectory()) {
            return 1;
        } else {
            return -1;
        }
    }
}

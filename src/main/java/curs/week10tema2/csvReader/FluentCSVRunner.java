package curs.week10tema2.csvReader;

import java.util.Comparator;

public class FluentCSVRunner {
    public static void main(String[] args) {
        FluentCSVFile f = new FluentCSVFile();
        f.read().show().sortByName(new Comparator<CSVEntry>() {
            @Override
            public int compare(CSVEntry o1, CSVEntry o2) {
                return o1.getName().compareTo(o2.getName());
            }
        }).show().filterByName("Cristi");
    }
}

package curs.week10tema2.csvReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FluentCSVFile implements FluentCSV {

    private List<CSVEntry> csvEntries = new ArrayList<>();

    @Override
    public FluentCSV read() {
        try{
            //read from file with FileReader
            BufferedReader reader = new BufferedReader(new FileReader("C:/test/test.txt"));
            //read first line
            String line = reader.readLine();
            //if there is a line, split it to make it an array, then add each of them to a list (arrayList)
            while(line != null) {
                String[] tokens = line.split(",");
                csvEntries.add(new CSVEntry(tokens[0], Integer.valueOf(tokens[1]), Integer.valueOf(tokens[2]),tokens[3],tokens[4]));
                line = reader.readLine();
            }
            //close reader
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //for chaining other method calls to the same instance
        return this;
    }

    @Override
    public FluentCSV show() {
        //stream it, print every line (csvEntry) from the list (csvEntries)
        csvEntries.stream().forEach(csvEntry -> System.out.println(csvEntry));
        System.out.println();
        return this;
    }

    @Override
    public FluentCSV filterByName(String name) {
        //stream it, get the names that match the parameter, print them
        csvEntries.stream().filter(csvEntry -> csvEntry.getName().equals(name)).forEach(System.out::println);
        return this;
    }

    @Override
    public FluentCSV sortByName(Comparator<CSVEntry> comparator) {
        //stream them, sort them by name (alphabetically)
        csvEntries.stream().sorted(comparator);
        return this;
    }
}

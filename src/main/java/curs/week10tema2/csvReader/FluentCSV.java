package curs.week10tema2.csvReader;

import java.util.Comparator;
//fluent interface = interface whose methods have itself as a return type !!!
//can call methods one after another like x().y().z()
public interface FluentCSV {
    FluentCSV read();
    FluentCSV show();
    FluentCSV filterByName(String name);
    FluentCSV sortByName(Comparator<CSVEntry> comparator);
}

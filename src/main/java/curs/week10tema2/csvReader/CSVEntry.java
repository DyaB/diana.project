package curs.week10tema2.csvReader;

public class CSVEntry {
    private String name;
    private int age;
    private int salary;
    private String occupation;
    private String nationality;

    public CSVEntry(){}

    public CSVEntry(String name, int age, int salary, String occupation, String nationality){
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.occupation = occupation;
        this.nationality = nationality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "CSVEntry{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                ", occupation='" + occupation + '\'' +
                ", nationality='" + nationality + '\'' +
                '}';
    }
}

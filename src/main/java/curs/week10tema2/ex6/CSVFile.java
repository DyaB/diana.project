package curs.week10tema2.ex6;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class CSVFile {
    private List<Transaction> transactions = new ArrayList<>();
    private List<Trader> traders = new ArrayList<>();

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public List<Trader> getTraders() {
        return traders;
    }

    public void readTranzactions() {
        try {
            //read from file with FileReader
            //   BufferedReader reader = new BufferedReader(new FileReader("C:/test/test1.txt"));
            BufferedReader reader = new BufferedReader(new FileReader("./src/main/java/curs/week10tema2/ex6/test1.csv"));
            //read first line
            String line = reader.readLine();
            //if there is a line, split it to make it an array, then add each of them to a transactions (arrayList)
            while (line != null) {
                String[] tokens = line.split(",");
                transactions.add(new Transaction(Integer.valueOf(tokens[0]), Integer.valueOf(tokens[1]), new Trader(tokens[2], tokens[3])));
                line = reader.readLine();
            }
            //close reader
            System.out.println("Transactions: " + transactions);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readTraders() {
        try {
            //read from file with FileReader
            // BufferedReader reader = new BufferedReader(new FileReader("C:/test/test1.txt"));
            BufferedReader reader = new BufferedReader(new FileReader("./src/main/java/curs/week10tema2/ex6/test1.csv"));
            //read first line
            String line = reader.readLine();
            //if there is a line, split it to make it an array, then add each of them to a trader (arrayList)
            while (line != null) {
                String[] tokens = line.split(",");
                traders.add(new Trader(tokens[2], tokens[3]));
                line = reader.readLine();
            }
            //close reader
            System.out.println("Traders: " + traders);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //sorted from 2011 - year as parameter
    public List<Transaction> sortedFromYear(List<Transaction> list, int year) {
        return list.stream().filter(transaction -> transaction.getYear() == year).sorted().collect(toList());
    }

    //unique cities where traders work
    public List<String> uniqueWorkingCities(List<Transaction> list) {
        List<String> uniqueCityList = new ArrayList<>();
        Map<String, Long> occurrence = list.stream().map(transaction -> transaction.getTrader().getCity()).collect(Collectors.groupingBy(o -> o, LinkedHashMap::new, Collectors.counting()));
        for (Map.Entry<String, Long> m : occurrence.entrySet()) {
            if (m.getValue() == 1) uniqueCityList.add(m.getKey());
        }
        return uniqueCityList;
    }

    //traders from Cambridge - city as parameter - and sort descending by name
    public List<Transaction> sortByNameAndCity(List<Transaction> list, String city) {
        return list.stream().filter(transaction -> transaction.getTrader().getCity().equals(city)).sorted((o1, o2) -> o2.getTrader().getName().compareTo(o1.getTrader().getName())).collect(toList());
    }

    //return String of trader names sorted alphabetically and separated by comma
    public void traderSorted(List<Trader> traders) {
        System.out.print("Traders' names sorted alphabetically: ");
        traders.stream().sorted((o1, o2) -> o1.getName().compareTo(o2.getName())).forEach(trader -> System.out.print(trader.getName() + ","));
    }

    //any traders from Milan?
    public boolean tradersFromSomeCity(String city) {
        long fromCity = traders.stream().filter(trader -> trader.getCity().equals(city)).count();
        //  System.out.println("there are " + fromCity + " traders in " + city);
        return fromCity != 0;
    }

    //update transactions so traders from Milan are moved to Cambridge
    public void updateTransactions() {
        transactions.stream().filter(transaction -> transaction.getTrader().getCity().equals("Milan"))
                .forEach(transaction -> transaction.getTrader().setCity("Cambridge"));
        //System.out.println("After update: " + transactions);
    }

    //highest value in all transactions
    public int highestValue() {
//        Optional<Transaction> t = transactions.stream().max(new Comparator<Transaction>() {
//            @Override
//            public int compare(Transaction o1, Transaction o2) {
//                return o1.getValue() > o2.getValue() ? o1.getValue() : o2.getValue();
//            }
//        }).orElse();
//        return t.ifPresent(Transaction::getValue);

        return transactions.stream().map(Transaction::getValue).max((o1, o2) -> {
            if (o1 > o2) return 1;
            else if (o1 < o2) return -1;
            return 0;
        }).get();
    }

    //lowest transaction value
    public int lowestValue() {
        return transactions.stream().map(Transaction::getValue).min((o1, o2) -> {
            if (o1 > o2) return 1;
            else if (o1 < o2) return -1;
            return 0;
        }).get();

    }
}

package curs.week10tema2.ex6;

public class TransactionMain {
    public static void main(String[] args) {
        CSVFile file = new CSVFile();
        file.readTraders();
        file.readTranzactions();
        System.out.println("Transactions from 2011: " + file.sortedFromYear(file.getTransactions(), 2011));
        System.out.println("Unique working cities: " + file.uniqueWorkingCities(file.getTransactions()));
        System.out.println("Traders from Cambridge sorted descending by name: " + file.sortByNameAndCity(file.getTransactions(), "Cambridge"));
        System.out.println("There are traders in Milan: " + file.tradersFromSomeCity("Milan"));
        file.updateTransactions();
        file.readTranzactions();
        System.out.println("Highest transaction value is: " + file.highestValue());
        System.out.println("Lowest transaction value is: " + file.lowestValue());
        //  System.out.println(file.tradersFromSomeCity("Constanta"));
         file.traderSorted(file.getTraders());
    }
}

package curs.week10tema2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.*;

public class StringList {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("aaa");
        list.add("abc");
        list.add("dwafwaggwa");
        list.add("abcade");
        list.add("ffff");
        System.out.println(startWithA(list));

        List<Integer> intList = new ArrayList<>(Arrays.asList(new Integer[]{1, 2, 3}));
        System.out.println(separatedString(intList));

        String oc = "abcdeafafa";
        occurenceInString(oc);
        List<String> stringList = new ArrayList<>(Arrays.asList(new String[]{"aba", "ces", "dadadwadra"}));
        listOfOccurenceStrings(stringList);
        wordCount(stringList);
        fizzBuzz(2,20);
    }

    public static List<String> startWithA(List<String> stringList) {
        return stringList.stream().filter(s -> s.substring(0, 1).equals("a") && s.length() == 3).collect(toList());
    }

    public static List<String> separatedString(List<Integer> integers) {
        return integers.stream().map(v -> v % 2 == 0 ? "e" + v : "o" + v).collect(toList());
    }

    public static void occurenceInString(String str) {
        Map<String,Long> result = Arrays.stream(str.split("")).map(s -> s.toLowerCase()).collect(Collectors.groupingBy(o -> o, LinkedHashMap::new, Collectors.counting()));
        System.out.println("The word and its char count: " + result);
    }

    public static void listOfOccurenceStrings(List<String> stringList){
        System.out.println("\n" + "List of words and occurence of chars in each word: ");
        stringList.stream().forEach(p->occurenceInString(p));
    }

    public static void wordCount(List<String> stringList){
        System.out.println("\n" + "Char count in all list of words: ");
        String str = stringList.stream().collect(Collectors.joining());
        occurenceInString(str);
    }

    public static void fizzBuzz(int start, int end){
        System.out.println("\n fizzbuzz problem from: " + start + "to" + end);
        IntStream.rangeClosed(start,end).forEach(p -> {
            if (p % 15 == 0) {
                System.out.println("fizzbuzz");
            } else {
                if (p % 3 == 0) {
                    System.out.println("fizz");
                } else {
                    if (p % 5 == 0) {
                        System.out.println("buzz");
                    } else {
                        System.out.println(p);
                    }
                }
            }
        });
    }

}

package curs.week9.personregistry;

public class DuplicateCnpException extends Exception {
    public DuplicateCnpException(String message) {
        super(message);
    }
}

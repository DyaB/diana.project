package curs.week9.personregistry;

import java.util.*;
import java.util.stream.Collectors;

public class PersonsRegistryImplStreams2 implements PersonsRegistry {

    private Map<Integer, Person> persons = new HashMap<>();

    @Override
    public void register(Person p) throws DuplicateCnpException {
        if (persons.get(p.getCnp()) != null) {
            throw new DuplicateCnpException("Found duplicate cnp: " + persons.get(p.getCnp()));
        }
        persons.put(p.getCnp(), p);
    }

    @Override
    public void unregister(int cnp) {
        persons.remove(cnp);
    }

    @Override
    public Set<Person> allPersons() {
        //HashSet - unique persons (values = names, keys = cnp)
        return new HashSet<>(persons.values());
    }

    @Override
    public int count() {
        //possible solution with stream:
        //  return (int) persons.keySet().stream().count();

        //but in our case regular method makes more sense:
        return persons.size();
    }

    @Override
    public Set<Integer> cnps() {
        return persons.keySet().stream().collect(Collectors.toSet());
        //   return persons.keySet();
    }

    @Override
    public Set<String> uniqueNames() {
        //good/nice use of stream operations here (map() method)
        return persons.values().stream()
                .map(p -> p.getName())
                //.collect(Collectors.toSet()); //this would give as a regular HashSet
                .collect(Collectors.toCollection(TreeSet::new)); //! but we want the TreeSet implementation of Set (to have names sorted in it)
    }

    @Override
    public Person findByCnp(int cnp) {
        //in our case this regular solution if shortest/fastest:
        //return persons.get(cnp);

        //But WHAT IF our persons collection was not a Map (or was not indexed by cnp) ??...
        //Then some streams operation could come in handy (filter, findAny..):

        //a functional solution using streams and looking just at the values (ignoring the Map keys)
        return persons.values().stream()
                .filter(e -> e.getCnp() == cnp) //select only ones with matching CNP
                .findAny() //try get the first one (returns an Optional, with value None if filter result was an empty collection)
                .orElse(null); //get the result from Optional wrapper, or 'null' as default (if Optional was empty)
    }

    @Override
    public Set<Person> findByName(String name) {
        //another nice example of using streams (filter)
        return persons.values().stream()
                .filter(n -> n.getName().equalsIgnoreCase(name))
                .collect(Collectors.toSet());
    }

    @Override
    public double averageAge() {
        /*
        //some possible solution with streams and .reduce (but more advance)
        int ageSum = persons.values().stream()      //-> Stream<Person>
                .map(Person::getAge)                //-> Stream<Integer>
                .reduce(0, (age1, age2) -> age1 + age2); //one way to reduce all values to a single one - with reduce() method (here with a function which just sums 2 values to combine then, and starts with 0 for empty stream case
        //or use reduce directly like this:
        int ageSum2 = persons.values().stream()
                .reduce(0, (sum, p) -> sum + p.getAge(), (a1, a2) -> a1 + a2); //another method to reduce directly from Person object to sum of their ages
        //and then just use the sum
        return (double) ageSum / persons.size();
        */

        /*
        //Solution using just map(), and then specialized .sum() method to reduce to sum:
        int ageSum3 = persons.values().stream() //-> Stream<Person>
                .mapToInt(Person::getAge)       //similar to Map, but instead of Stream<Integer> produces an IntStream (stream of primitive int)
                .sum(); //and IntStream has then some nice methods, like sum, max, min, average ...
        return (double) ageSum3 / persons.size(); //TO DO: actually wrong here, division by zero when persons is empty, need to handle that case too!
        */

        //Similar but shorter solution - as IntStream also has the useful .average() method!
        return persons.values().stream()    //-> Stream<Person>
                .mapToInt(Person::getAge)   //-> IntStream
                .average()                  //-> Optional<Double> (is None when persons size is empty!)
                .orElse(0);           //-> use default of 0 when persons was empty
    }

    @Override
    public double adultsPercentage() {
        long adults = persons.values().stream() //Stream<Person>
                .mapToInt(Person::getAge)       //IntStream
                .filter(a -> a >= 18)           //IntStream (of only values >=18)
                .count();
        return persons.isEmpty() ?
                0 :
                adults * 100.0 / persons.size();
    }

    @Override
    public double adultsWhoVotedPercentage() {
        List<Person> adults = persons.values().stream() //Stream<Person>
                .filter(p -> p.getAge() >= 18)          //Stream<Person> (only adults)
                .collect(Collectors.toList());

        long adultsCount = adults.size();
        long votersCount = adults.stream()
                .filter(Person::isHasVoted)
                .count();

        return adultsCount == 0 ? 0 : votersCount * 100.0 / adultsCount;
    }

    //just to test the class a little
    public static void main(String[] args) throws DuplicateCnpException {
        PersonsRegistry r = new PersonsRegistryImplStreams2();
        r.register(new Person(1111, "Ionel", 22, true));
        r.register(new Person(2222, "Ana", 16, false));
        r.register(new Person(3333, "Maria", 30, false));
        r.register(new Person(4444, "Dan", 50, true));
        r.register(new Person(5555, "Dan", 14, false));
        System.out.println(r.cnps());
        System.out.println("uniqueNames(): " + r.uniqueNames());
        System.out.println("findByName('dan'): " + r.findByName("dan"));
        System.out.println("findByCnp(3333): " + r.findByCnp(3333));
        System.out.println("adults(%): " + r.adultsPercentage() + "%");
        System.out.println("adult voters(%): " + r.adultsWhoVotedPercentage());
        System.out.println("averageAge: " + r.averageAge());
    }
}

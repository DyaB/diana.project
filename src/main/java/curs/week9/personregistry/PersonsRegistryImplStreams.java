package curs.week9.personregistry;


import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class PersonsRegistryImplStreams implements PersonsRegistry {

    private Map<Integer, Person> persons = new HashMap<>();

    @Override
    public void register(Person p) throws DuplicateCnpException {
        if (persons.get(p.getCnp()) != null) {
            throw new DuplicateCnpException("Found duplicate cnp: " + persons.get(p.getCnp()));
        }
        persons.put(p.getCnp(), p);
    }

    @Override
    public void unregister(int cnp) {
        persons.remove(cnp);
    }

    @Override
    public Set<Person> allPersons() {
        return new HashSet<>(persons.values());
    }

    @Override
    public int count() {
        return persons.keySet().size();
    }

    @Override
    public Set<Integer> cnps() {
        return persons.keySet();
    }

    @Override
    public Set<String> uniqueNames() {
        return persons.values().stream()
                .map(Person::getName)
                .collect(toSet());
    }

    @Override
    public Person findByCnp(int cnp) {
        /*        This is one way to look at it... */
//        Set<Integer> keySet = persons.keySet();
//        Optional<Integer> cnpOptional = keySet.stream().filter(c -> c == cnp).findFirst();
//        if (cnpOptional.isPresent())
//            return persons.get(cnpOptional.get());
//        else
//            return null;


        List<Map.Entry<Integer, Person>> personEntries =
                persons.entrySet().stream()
                        .filter(e -> e.getKey() == cnp)
                        .collect(toList());

        if (!personEntries.isEmpty()) {
            return personEntries.get(0).getValue();
        } else {
            return null;
        }
    }

    @Override
    public Set<Person> findByName(String name) {
        return persons.values().stream()
                .filter(n -> n.getName().equalsIgnoreCase(name))
                .collect(toSet());
    }

    @Override
    public double averageAge() {
        return 0;
    }

    @Override
    public double adultsPercentage() {
        return persons.values().stream()
                .filter(person -> person.getAge() >= 18)
                .count()
                * 100.0 / persons.size();
    }

    @Override
    public double adultsWhoVotedPercentage() {
        List<Person> adults = persons.values().stream()
                .filter(person -> person.getAge() >= 18)
                .collect(toList());

        return adults.stream()
                .filter(Person::isHasVoted)
                .count()
                * 100.0 / adults.size();
    }

    public static void main(String[] args) throws DuplicateCnpException {
        PersonsRegistry r = new PersonsRegistryImplStreams();
        r.register(new Person(1111, "Ionel", 22, true));
        r.register(new Person(2222, "Ana", 16, false));
        r.register(new Person(3333, "Maria", 30, false));
        r.register(new Person(4444, "Dan", 50, true));
        r.register(new Person(5555, "Dan", 14, false));

        System.out.println("uniqueNames(): " + r.uniqueNames());
        System.out.println("findByName('dan'): " + r.findByName("dan"));
        System.out.println("findByCnp(3333): " + r.findByCnp(3333));
        System.out.println("adults(%): " + r.adultsPercentage() + "%");
        System.out.println("adult voters(%): " + r.adultsWhoVotedPercentage());
        System.out.println("averageAge: " + r.averageAge());
    }

}

package curs.week9.personregistry;

public class Person {
    private int cnp;
    private String name;
    private int age;
    private boolean hasVoted;

    public Person(int cnp, String name, int age, boolean hasVoted) {
        this.cnp = cnp;
        this.name = name;
        this.age = age;
        this.hasVoted = hasVoted;
    }

    public int getCnp() {
        return cnp;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean isHasVoted() {
        return hasVoted;
    }

    @Override
    public String toString() {
        return "Person{" +
                "cnp=" + cnp +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", hasVoted=" + hasVoted +
                '}';
    }
}

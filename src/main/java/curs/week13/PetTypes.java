package curs.week13;

public class PetTypes {
    private int id;
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PetTypes{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}

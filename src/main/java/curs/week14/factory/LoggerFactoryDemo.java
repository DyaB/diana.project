package curs.week14.factory;

import curs.week14.factory.logger.Logger;
import curs.week14.factory.logger.LoggerFactory;
import static curs.week14.factory.logger.LoggerFactory.LoggerType.*;

public class LoggerFactoryDemo {
    public static void main(String[] args) {
//        Logger logger1= LoggerFactory.getLogger(CONSOLE);
//        logger1.error("error");
        LoggerFactory.getLogger(FILE).info("info");
  //      LoggerFactory.getLogger(DB).warn("db");
        LoggerFactory.getLogger(CONSOLE).warn("warning");
        LoggerFactory.getLogger(CONSOLE).info("info");
        LoggerFactory.getLogger(CONSOLE).error("error");
    }
}

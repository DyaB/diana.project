package curs.week14.factory.logger;

public class ConsoleLogger implements Logger {
    public ConsoleLogger() {
        System.out.println("new consoleLogger instance created!!!!");
    }

    @Override
    public void info(String s) {
        System.out.println("[info] :" + s);
    }

    @Override
    public void warn(String s) {
        System.out.println("[warn] : " + s);
    }

    @Override
    public void error(String s) {
        System.out.println("[error] :" + s);
    }
}

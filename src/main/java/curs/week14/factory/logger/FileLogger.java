package curs.week14.factory.logger;

public class FileLogger implements Logger{
    public FileLogger() {
        System.out.println("new FileLogger instance created!!!!!");
    }

    @Override
    public void info(String s) {
        System.out.println("printing INFO to file: " + s);
    }

    @Override
    public void warn(String s) {
        System.out.println("printing WARN to file: " + s);
    }

    @Override
    public void error(String s) {
        System.out.println("printing ERROR to file: " + s);
    }
}

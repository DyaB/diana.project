package curs.week14.factory.logger;

public class LoggerFactory {
    public enum LoggerType{
        CONSOLE,
        FILE,
        DB
        }

//    private static Logger consoleLogger;// = new ConsoleLogger();
//    private static Logger fileLogger;// = new FileLogger();

    private LoggerFactory() {
    }

    public static Logger getLogger(LoggerType loggerType){
        switch (loggerType){
            case FILE:
                return FileLoggerHolder.INSTANCE;
//                if(fileLogger==null)
//                    fileLogger = new FileLogger();
//                return fileLogger;
               // break; nu e nevoie pt ca are return
            case CONSOLE:
                return ConsoleLoggerHolder.INSTANCE;
//                if(consoleLogger==null)
//                    consoleLogger = new ConsoleLogger();
//                return consoleLogger;
            default:
                throw new RuntimeException("Logger type not supported: " + loggerType);
        }
    }

    private static class ConsoleLoggerHolder{
        private static Logger INSTANCE = new ConsoleLogger();
    }

    private static class FileLoggerHolder{
        private static Logger INSTANCE = new FileLogger();
    }

}

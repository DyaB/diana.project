package curs.week14.singleton;

public class LoggerDemo {
    public static void main(String[] args) {
        Logger logger = Logger.getInstance();
        logger.error("some error");
        logger.info("some message here");
        Logger logger2 = Logger.getInstance();
        logger2.warn("a warning");
        System.out.println("same? : "+ (logger==logger2));
    }
}

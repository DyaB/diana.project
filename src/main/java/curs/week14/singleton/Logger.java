package curs.week14.singleton;

public class Logger {
    private static Logger inst;
    static {
        inst = new Logger();
    }
    private Logger() {
        System.out.println("Creating Logger");
    }
    static Logger getInstance(){
        return inst;
    }



    void info(String s){
        System.out.println("[INFO] "+ s);
    }

    void warn(String s){
        System.out.println("[WARN] "+ s);
    }

    void error(String s){
        System.out.println("[ERROR] "+ s);
    }




}
